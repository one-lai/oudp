const tagsView = {
  state: {
    visitedViews: [{
      name:"workbench",
      title:'首页'
    }],
    cachedViews: []
  },
  mutations: {
    ADD_VISITED_VIEWS: (state, view) => {
      if (state.visitedViews.some(v => v.name === view.name)) return
      state.visitedViews.push(Object.assign({}, view, {
        title: view.meta.title || 'no-name'
      }))
      if (!view.meta.noCache) {
        state.cachedViews.push(view.name)
      }
    },
    DEL_VISITED_VIEWS: (state, view) => {
      for (const [i, v] of state.visitedViews.entries()) {
        if (v.path === view.path ) {
          state.visitedViews.splice(i, 1)
          break
        }

      }
      for (const i of state.cachedViews) {
        if (i === view.name ) {
          const index = state.cachedViews.indexOf(i)
          state.cachedViews.splice(index, 1)
          break
        }

      }
    },
    DEL_OTHERS_VIEWS: (state, view) => {
      state.visitedViews =state.visitedViews.filter((item,i)=>{
        console.log('item',item,i);
        if(item.name == 'workbench' || item.name == view.name){
          return true
        }

      })


      state.cachedViews =state.cachedViews.filter((item,i)=>{
        console.log('item',item,i);
        if(item.name == 'workbench' || item.name == view.name){
          return true
        }

      })


    },
    DEL_ALL_VIEWS: (state) => {
      // console.log(state.visitedViews);


      state.cachedViews=state.cachedViews.filter((item) =>{
        return item.name == 'workbench'
      })
      state.visitedViews=state.visitedViews.filter((item) =>{
        return item.name == 'workbench'
      })

      // console.log(state.cachedViews);
      // console.log(state.visitedViews);


    }
  },
  actions: {
    addVisitedViews({ commit }, view) {
      commit('ADD_VISITED_VIEWS', view)
    },
    delVisitedViews({ commit, state }, view) {
      return new Promise((resolve) => {
        commit('DEL_VISITED_VIEWS', view)
        resolve([...state.visitedViews])
      })
    },
    delOthersViews({ commit, state }, view) {
      return new Promise((resolve) => {
        commit('DEL_OTHERS_VIEWS', view)
        resolve([...state.visitedViews])
      })
    },
    delAllViews({ commit, state }) {
      return new Promise((resolve) => {
        console.log('state',state);

        commit('DEL_ALL_VIEWS')
        resolve([...state.visitedViews])
      })
    }
  }
}

export default tagsView
