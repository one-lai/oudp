'use strict'
module.exports = {
  NODE_ENV: '"production"',
  VERSION: '"1.1.1"',
  INFO: '"-Beta build ' + new Date().toLocaleString() + ' - ' + process.env.USERNAME + ', ' + process.env.USERDOMAIN + '"',
  BASE_API: '"/base_data"',
  CONTEXT_PATH: '"/"'
}