import router from './router'
import store from './store'
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'
import { Message } from 'element-ui'
import { getToken } from '@/utils/auth'

const whiteList = ['/login', '/404', '/faild', '/redirect']

// router.beforeEach((to, from, next) => {
//   // NProgress.start()
//   next()
// })

router.beforeEach((to, from, next) => {
  NProgress.start()
  if (getToken()) {
    if (store.getters.roles.length === 0) {
      store.dispatch('GetInfo').then(resp => {
        // 拉取用户信息, 设置权限
        // add you code here
        // 模块及按钮json对象
        const basicData = router.options.routes.findIndex(r => { return r.name === 'BasicData' })
        const unitData = router.options.routes.findIndex(r => { return r.name === 'unitData' })
        const mods = resp.result.data.modules[1].children[0]
        // console.log(Object.assign({},mods))

        if (mods.moduleName === router.options.routes[basicData].meta.title ) {
          const baseDataModules = mods.children 

           var baseRouter = router.options.routes[basicData].children.concat(router.options.routes[unitData].children)
          // console.log(Object.assign({}, baseRouter))

          for (var i = 0; i < baseDataModules.length; i++) {
            // 一级菜单：基础数据档案
            const m = baseDataModules[i]
            for (var j = 0; j < baseRouter.length; j++) {
              // 二级菜单
              var r = baseRouter[j]
              if (r.meta.title.trim() === m.moduleName.trim()) {
                // 放开菜单
                r.hidden = false

                // r.meta.p = ''
                r.meta.p = []
                // 设置按钮
                if (m.children && m.children.length > 0) {
                  // 有按钮权限定义
                  // console.log(Object.assign({}, m))
                  for (var k = 0; k < m.children.length; k++) {
                    var btn = m.children[k]
                    // console.log(Object.assign({}, btn))
                    if (btn.moduleName === '新增') {
                      // r.meta.p += 'c'
                      r.meta.p.push('c')
                    }
                    if (btn.moduleName === '编辑') {
                      // r.meta.p += 'u'
                      r.meta.p.push('u')
                    }
                    if (btn.moduleName === '删除') {
                      // r.meta.p += 'd'
                      r.meta.p.push('d')
                    }
                    if (btn.moduleName === '批量删除') {
                      // r.meta.p += 'ds'
                      r.meta.p.push('ds')
                    }
                    if (btn.moduleName === '详情') {
                      // r.meta.p += 'r'
                      r.meta.p.push('r')
                    }
                    if (btn.moduleName === '导入') {
                      // r.meta.p += 'i'
                      r.meta.p.push('i')
                    }
                    if (btn.moduleName === '导出') {
                      // r.meta.p += 'e'
                      r.meta.p.push('e')
                    }
                    if (btn.moduleName === '开通账号') {
                      // r.meta.p += 'e'
                      r.meta.p.push('oa')
                    }
                  }
                }// end of btn
              }
            }
          }
        }
        if (to.path === '/login') {
          next({ path: '/' })
          NProgress.done() // if current page is dashboard will not trigger	afterEach hook, so manually handle it
        } else {
          next()
        }
        NProgress.done()
      }).catch((err) => {
        console.log('err->', err)
        store.dispatch('FedLogOut').then(() => {
          Message.error(err || 'Verification failed, please login again')
          next({ path: '/' })
        })
      })
    } else {
      NProgress.done()
      next()
    }
  } else {
    if (whiteList.indexOf(to.path) !== -1) {
      next()
    } else {
      next('/login')
    }
    NProgress.done()
  }
})

router.afterEach(() => {
  NProgress.done()
})
