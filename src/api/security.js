import request from '@/utils/request'

export function getList(params) {
  return request({
    url: '/api/bsuser/bsUser/list',
    method: 'post',
    data: params,
    headers: { 'Content-Type': 'application/json' }
  })
}

export function saveList(params) {
  console.log(params)
  return request({
    url: '/api/bsuser/bsUser/save',
    method: 'post',
    data: params,
    headers: { 'Content-Type': 'application/json' }
  })
}

export function deleteItem(params) {
  return request({
    url: '/api/bsuser/bsUser/delete',
    method: 'post',
    data: params,
    headers: { 'Content-Type': 'application/json' }
  })
}

export function deleteItems(params) {
  console.log(params)
  return request({
    url: '/api/bsuser/bsUser/deleteAll',
    method: 'post',
    data: params,
    headers: { 'Content-Type': 'application/json' }
  })
}

export function addUser(params) {
  console.log(params)
  return request({
    url: '/api/bsuser/bsUser/addUser',
    method: 'post',
    data: params,
    headers: { 'Content-Type': 'application/json' }
  })
}
