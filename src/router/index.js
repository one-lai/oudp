import Vue from 'vue'
import Router from 'vue-router'

// in development-env not use lazy-loading, because lazy-loading too many pages will cause webpack hot update too slow. so only in production use lazy-loading;
// detail: https://panjiachen.github.io/vue-element-admin-site/#/lazy-loading

Vue.use(Router)

/* Layout */
import Layout from '../views/layout/Layout'

/**
* hidden: true                   if `hidden:true` will not show in the sidebar(default is false)
* alwaysShow: true               if set true, will always show the root menu, whatever its child routes length
*                                if not set alwaysShow, only more than one route under the children
*                                it will becomes nested mode, otherwise not show the root menu
* redirect: noredirect           if `redirect:noredirect` will no redirect in the breadcrumb
* name:'router-name'             the name is used by <keep-alive> (must set!!!)
* meta : {
    title: 'title'               the name show in submenu and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar,
  }
**/
const basicData = [{
    path: 'elevator',
    name: 'elevator',
    component: () => import('@/views/data/basicData/elevator/elevator'),
    meta: {
      title: '电梯',
      icon: 'lift'
    }
  },

  {
    path: 'pos',
    name: 'ProductSales',
    component: () => import('@/views/data/basicData/pos/pos'),
    meta: {
      title: '销售数据',
      icon: 'tree'
    }
  },
  {
    path: 'product',
    name: 'Product',
    component: () => import('@/views/data/basicData/product/ProductList'),
    meta: {
      title: '产品数据',
      icon: 'product'
    }
  },
  {
    path: 'brand',
    name: 'brand',
    component: () => import('@/views/data/basicData/brand/brand'),
    meta: {
      title: '品牌档案',
      icon: 'brand'
    }
  },
  {
    path: 'technician',
    name: 'Technician',
    component: () => import('@/views/data/basicData/technician/technician'),
    meta: {
      title: '技师',
      icon: 'tech'
    }
  },
  {
    path: 'place',
    name: 'Place',
    component: () => import('@/views/data/basicData/place/place'),
    meta: {
      title: '使用场所',
      icon: 'place'
    }
  },
  {
    path: 'security',
    name: 'Security',
    component: () => import('@/views/data/basicData/security/security'),
    meta: {
      title: '安全员',
      icon: 'guard'
    }
  },
]
const unitData = [{
    type: 2,
    path: 'bsmaintaintpl',
    name: 'bsmaintaintpl',
    component: () => import('@/views/data/basicData/bsmaintaintpl/pagelist'),
    meta: {
      title: '保养模板信息',
      icon: 'template'
    }
  },
  {
    type: 2,
    path: 'supervisoryUnit',
    name: 'SupervisoryUnit',
    component: () => import('@/views/data/basicData/supervisoryUnit/list'),
    meta: {
      title: '监察单位',
      icon: 'units'
    }
  },
  {
    type: 2,
    path: 'thirdInstitution',
    name: 'ThirdInstitution',
    component: () => import('@/views/data/basicData/thirdInstitution/list'),
    meta: {
      title: '第三方检验机构',
      icon: 'zhijian'
    }
  },
  {
    type: 2,
    path: 'manufacturer',
    name: 'Manufacturer',
    component: () => import('@/views/data/basicData/manufacturer/list'),
    meta: {
      title: '生产厂商',
      icon: 'tree'
    }
  },
  {
    type: 2,
    path: 'maintenance',
    name: 'Maintemence',
    hidde: true,
    component: () => import('@/views/data/basicData/maintenance/list'),
    meta: {
      title: '维保单位',
      icon: 'units'
    }
  },
  {
    type: 2,
    path: 'useUnit',
    name: 'useUnit',
    component: () => import('@/views/data/basicData/useUnit/useUnit'),
    meta: {
      title: '使用单位',
      icon: 'inuse'
    }
  },
  {
    type: 2,
    path: 'rescurePoint',
    name: 'RescurePoint',
    component: () => import('@/views/data/basicData/rescurePoint/list'),
    meta: {
      title: '志愿救援点',
      icon: 'units'
    }
  },
  {
    type: 2,
    path: 'gdTemplate',
    name: 'GdTemplate',
    component: () => import('@/views/data/basicData/gdTemplate/list'),
    meta: {
      title: '评分档案',
      icon: 'inuse'
    }
  }

]
export const constantRouterMap = [{
    path: '/login',
    component: () => import('@/views/login/Login'),
    hidden: true
  },
  {
    path: '/faild',
    component: () => import('@/views/login/LoginFaild'),
    hidden: true
  },
  {
    path: '/redirect',
    component: () => import('@/views/login/Redirect'),
    hidden: true
  },
  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },
  {
    path: "/",
    component: Layout,
    redirect: "/dashboard",
    name: "Dashboard",
    hidden: false,
    meta: {
      title: "首页",
      icon: "work"
    }
  },

  {
    path: '',
    component: Layout,
    name: 'dashboard',
    alwaysShow: false,
    meta: {
      title: '首页',
      icon: "home",

    },
    children: [{
      path: "dashboard",
      name: "workbench",
      component: () => import("@/views/welcome.vue"),
      meta: {
        title: "首页",
        id: "040101",
        icon: "home",

      }
    }]

  },
  {
    path: '/redirect2',
    component: Layout,
    hidden: true,
    children: [
      {
        path: '/redirect2/:path*',
        component: () => import('@/views/data/redirect/index')
      }
    ]
  },
  {
    path: '/basicData',
    component: Layout,
    redirect: 'noredirect',
    name: 'BasicData',
    alwaysShow: true,
    meta: {
      title: '基础数据档案',
      icon: 'dossier'
    },
    children: basicData.filter(v => {
      return true
    })
  },
  {
    path: '/basicData',
    component: Layout,
    redirect: 'noredirect',
    name: 'unitData',
    alwaysShow: true,
    meta: {
      title: '单位档案',
      icon: 'table'
    },
    children: unitData
  }

]

export default new Router({
  // mode: 'history', //后端支持可开
  scrollBehavior: () => ({
    y: 0
  }),
  routes: constantRouterMap
})
