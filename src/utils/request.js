import axios from 'axios'
import {
  Message,
  MessageBox
} from 'element-ui'
import store from '../store'
import {
  getToken
} from '@/utils/auth'
import respc from './respc'

// 创建axios实例
const service = axios.create({
  baseURL: process.env.BASE_API, // api的base_url
  timeout: 10000, // 请求超时时间
  headers: {
    'Content-Type': 'application/json'
  }
})
var loginStatus = false;
// request拦截器
service.interceptors.request.use(config => {
  if (store.getters.token) {
    // config.headers['Content-Type'] = 'application/json'
    config.headers['X-Token'] = getToken() // 让每个请求携带自定义token 请根据实际情况自行修改
  }
  return config
}, error => {
  // Do something with request error
  console.log(error)
  Promise.reject(error)
})

// respone拦截器
service.interceptors.response.use(
  response => {
    const res = response.data
    if (res.code === 1) {
      loginStatus = true
      if (response.data.result === '') {
        response.data.result = []
      }
      return response.data
    } else if (res.code === respc.USER_NOT_LOGGED_IN.code) {
      const reqpath = location.href.split('#')[1]
      if (reqpath != '/login') {
        if (loginStatus)
          MessageBox.alert('所访问的页面需要登录！', '系统提示', {
            confirmButtonText: '确定',
            type: 'warning',
            center: true
          }).then(() => {
            store.dispatch('FedLogOut').then(() => {
              location.reload()
            })
          })
      }
    } else if (res.code == respc.USER_HAS_EXISTED.code) {
      MessageBox.alert('此手机号已存在！', '系统提示', {
        confirmButtonText: '确定',
        type: 'warning',
        center: true
      }).then(() => {
        store.dispatch('FedLogOut').then(() => {
          // location.reload()
        })
      })
    } else {
      MessageBox.alert('未知错误，请联系系统管理员', '系统提示', {
        confirmButtonText: '确定',
        type: 'warning',
        center: true
      }).then(() => {
        store.dispatch('FedLogOut').then(() => {
          location.reload()
        })
      })
    }
  },
  error => {
    console.log('err', error)
    // Message({
    //   dangerouslyUseHTMLString: true,
    //   message: error.response.status + ': ' + error.response.statusText,
    //   type: 'error',
    //   duration: 10 * 1000
    // })
    return Promise.reject(error)
  }
)

export default service