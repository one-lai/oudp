import More from '../packages/more/index.js';
import Perm from '../packages/perm/index.js';
import Search from '../packages/search/index.js';
import Zone from '../packages/zone/index.js';
import ZoneArea from '../packages/zone-area/index.js';
import ZoneView from '../packages/zone-view/index.js';
import Filter from '../packages/filter/index.js';
import ElSift from '../packages/sift/index.js';
import Layout from '../packages/layout/index.js';
import Ocharts from '../packages/ocharts/index.js';
import Tree from '../packages/tree/index.js';
import DateSelect from '../packages/date-select/index.js';
import LineBtn from '../packages/linebtn/index.js';
import Operate from '../packages/operate/index.js';


const components = [More,ElSift,Operate,Perm,Search,Zone,ZoneArea,ZoneView,Filter,Tree,Ocharts,LineBtn,DateSelect,Layout.VerticalLayout]


const ElementExt =   {
    install:function(Vue, opts = {}) {
        components.forEach(component => {
            Vue.component(component.name, component);
        });
        }
}

export default ElementExt
