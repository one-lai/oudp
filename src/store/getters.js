const getters = {
  sidebar: state => state.app.sidebar,
  device: state => state.app.device,
  token: state => state.user.token,
  visitedViews: state => state.tagsView.visitedViews,
  avatar: state => state.user.avatar,
  name: state => state.user.name,
  roles: state => state.user.roles,
  month: state => state.month.date,
  zoneCache: state => state.zoneCache.zone,
  excludeArr: state => state.updata.ExcludeArr,

}
export default getters