import Operate from '../operate/operate'
import methods from './methods'
Operate.install = function (Vue) {
  Vue.component(Operate.name, Main)
}
Operate._mixinsMethods = methods

export default Operate
