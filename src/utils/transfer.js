import axios from 'axios'
import store from '../store'
import { getToken } from '@/utils/auth'
import qs from 'qs'

import { Message } from 'element-ui'
// 创建axios实例
const service = axios.create({
  baseURL: process.env.BASE_API,
  timeout: 50000,
  responseType: 'blob',
  headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
  transformRequest: [function(data) {
    return qs.stringify(data)
  }]
})

// request拦截器
service.interceptors.request.use(config => {
  if (store.getters.token) {
    // config.headers['Content-Type'] = 'application/json'
    config.headers['X-Token'] = getToken() // 让每个请求携带自定义token 请根据实际情况自行修改
  }
  return config
}, error => {
  // Do something with request error
  console.log(error) // for debug
  Promise.reject(error)
})

// respone拦截器
service.interceptors.response.use(
  resp => {
    var filename = resp.headers['content-disposition'].split('=')[1]
    filename = decodeURI(filename, 'utf-8')
    const blob = new Blob([resp.data])
    const url = window.URL.createObjectURL(blob)
    const link = document.createElement('a')
    link.style.display = 'none'
    link.href = url
    // link.target = '_blank'
    link.setAttribute('download', filename)
    document.body.appendChild(link)
    link.click()
  },
  error => {
    console.log('err->', error) // for debug
    Message({
      message: error.response.status + ': ' + error.response.statusText,
      type: 'error',
      duration: 5 * 1000
    })
    return Promise.reject(error)
  }
)

export default service
