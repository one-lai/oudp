import VerticalLayout from "./src/vertical";
import HorizontalLayout from "./src/horizontal";


export default {
  VerticalLayout,
  HorizontalLayout
};