import {
  login,
  logout,
  getInfo,
  logon
} from '@/api/login'
import {
  getToken,
  setToken,
  removeToken
} from '@/utils/auth'

const user = {
  state: {
    token: getToken(),
    name: '',
    avatar: '',
    roles: [],
    modules: []
  },

  mutations: {
    SET_TOKEN: (state, token) => {
      state.token = token
    },
    SET_NAME: (state, name) => {
      state.name = name
    },
    SET_AVATAR: (state, avatar) => {
      state.avatar = avatar
    },
    SET_ROLES: (state, roles) => {
      state.roles = roles
    },
    SET_MODULES: (state, modules) => {
      state.modules = modules
    }
  },

  actions: {
    // 获取 sso 登录信息
    Logon({
      commit
    }) {
      return new Promise((resolve, reject) => {
        logon().then(response => {
          const data = response.result.data
          setToken(data.id)
          commit('SET_TOKEN', data.id)
          commit('SET_NAME', data.name)
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // @Deprecated
    // 登录，获取sso登录地址
    Login({
      commit
    }, userInfo) {
      const username = userInfo.username.trim()
      return new Promise((resolve, reject) => {
        login(username, userInfo.password).then(response => {
          resolve()
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 获取用户信息
    GetInfo({
      commit,
      state
    }) {
      return new Promise((resolve, reject) => {
        getInfo(state.token).then(response => {
          const data = response.result.data
          if (data.name) {
            commit('SET_NAME', data.name)
          } else {
            removeToken()
          }

          if (data.roles && data.roles.length > 0) { // 验证返回的roles是否是一个非空数组
            commit('SET_ROLES', data.roles)
          } else {
            reject('getInfo: roles must be a non-null array !')
          }
          if (data.modules && data.modules.length > 0) {
            commit('SET_MODULES', data.modules)
          } else {
            reject('getInfo: modules must be a non-null array !')
          }
          commit('SET_AVATAR', data.avatar)

          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 登出
    LogOut({
      commit,
      state
    }) {
      return new Promise((resolve, reject) => {
        logout(state.token).then(response => {
          commit('SET_NAME', '')
          commit('SET_TOKEN', '')
          commit('SET_ROLES', [])
          commit('SET_MODULES', [])
          removeToken()
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 前端 登出
    FedLogOut({
      commit
    }) {
      return new Promise(resolve => {
        commit('SET_TOKEN', '')
        removeToken()
        resolve()
      })
    }
  }
}

export default user