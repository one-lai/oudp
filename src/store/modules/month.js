
const month = {
  state: {
    date: {
        type:'',
        month:''
    },
  },
  mutations: {

    PUSH_DATE: (state, date) => {
      state.date = date
    }
  },
  actions: {

    Push_Date({ commit }, date) {
      commit('PUSH_DATE', date)
    }
  }
}

export default month
