import request from '@/utils/request'
import transfer from '@/utils/transfer'

export function getList(params) {
  return request({
    url: '/api/bsuser/bsUser/list',
    method: 'post',
    data: params,
    headers: {
      'Content-Type': 'application/json'
    }
  })
}

export function saveList(params) {
  console.log(params)
  return request({
    url: '/api/bsuser/bsUser/save',
    method: 'post',
    data: params,
    headers: {
      'Content-Type': 'application/json'
    }
  })
}


export function seeDetail(params) {
  console.log(params)
  return request({
    url: '/api/bsuser/bsUser/detail',
    method: 'post',
    data: params,
    headers: {
      'Content-Type': 'application/json'
    }
  })
}

export function deleteItem(params) {
  return request({
    url: '/api/bsuser/bsUser/delete',
    method: 'post',
    data: params,
    headers: {
      'Content-Type': 'application/json'
    }
  })
}

export function deleteItems(params) {
  console.log(params)
  return request({
    url: '/api/bsuser/bsUser/deleteAll',
    method: 'post',
    data: params,
    headers: {
      'Content-Type': 'application/json'
    }
  })
}

export function exportData(params) {
  return transfer({
    method: 'post',
    url: '/api/bsuser/bsUser/export',
    data: params
  })
}

export function exportDataExactly(params) {
  return transfer({
    method: 'post',
    url: '/api/bsuser/bsUser/exportExactly',
    data: params
  })
}

export function getTemplate() {
  return transfer({
    method: 'post',
    url: '/api/bsuser/bsUser/import/template',
    data: ''
  })
}

export function getTemplateTechnician() {
  return transfer({
    method: 'post',
    url: '/api/bsuser/bsUser/import/templateTechnician',
    data: ''
  })
}

export function importExcel(params) {
  return request({
    url: '/api/bsuser/bsUser/import',
    method: 'post',
    data: params,
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

export function findCountByUserType(params) {
  return request({
    url: '/api/bsuser/bsUser/findCountAndDone',
    method: 'post',
    data: params,
    headers: {
      'Content-Type': 'application/json'
    }
  })
}

export function sift(params) {
  return request({
    url: '/api/bsuser/bsUser/sift',
    method: 'post',
    data: params
  })
}