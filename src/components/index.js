
import Ogrid from '@/components/Ogrid/src/main'
import TopBtn from '@/components/Ogrid/TopBtn/index.vue'

Vue.component('ogrid',Ogrid)
Vue.component('topBtn',TopBtn)