import request from '@/utils/request'

export function getList(params) {
  return request({
    url: '/api/bsorganization/bsOrganization/list',
    method: 'post',
    data: params
  })
}

export function saveList(params) {
  return request({
    url: '/api/bsorganization/bsOrganization/save',
    method: 'post',
    data: params,
    headers: {'Content-Type': 'application/json'}
  })
}

export function deleteItem(params) {
  return request({
    url: '/api/bsorganization/bsOrganization/delete',
    method: 'post',
    data: params,
    headers: {'Content-Type': 'application/json'}
  })
}

export function deleteItems(params) {
  console.log(params)
  return request({
    url: '/api/bsorganization/bsOrganization/deleteAll',
    method: 'post',
    data: params,
    headers: {'Content-Type': 'application/json'}
  })
}

export function seeDetail(params) {
  console.log(params)
  return request({
    url: '/api/bsorganization/bsOrganization/detail',
    method: 'post',
    data: params,
    headers: {'Content-Type': 'application/json'}
  })
}

export function exportData(params) {
  return transfer({
    method: 'post',
    url: '/api/bsorganization/bsOrganization/export',
    data: params
  })
}

export function getTemplate() {
  return transfer({
    method: 'post',
    url: '/api/bsorganization/bsOrganization/import/template',
    data: ''
  })
}

export function checkOrganizationName(params) {
  return request({
    url: '/api/bsorganization/bsOrganization/checkOrganizationName',
    method: 'post',
    data: params,
    headers: { 'Content-Type': 'application/json' }
  })
}
