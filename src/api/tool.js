import request from '@/utils/request'
import transfer from '@/utils/transfer'
// 查询区域编码
export function getSubAreaByPid(params) {
  return request({
    url: '/api/tools/framework/getSubAreaByPid',
    method: 'post',
    data: params
  })
}

// 单个个dictType数据字典查询接口
export function getDictsByType(params) {
  return request({
    url: '/api/tools/framework/getDictsByType',
    method: 'post',
    data: params
  })
}

// 多个dictType数据字典查询接口
export function getDictsByTypeList(params) {
  return request({
    url: '/api/tools/framework/getDictsByTypeList',
    method: 'post',
    data: params
  })
}

/**
 * 一个类型多个子类字典
 * @param params
 */
export function getDictsChildenByType(params) {
  return request({
    url: '/api/tools/framework/getDictsChildenByType',
    method: 'post',
    data: params
  })
}

export function getZone(params) {
  return request({
    url: '/api/tools/framework/getZone',
    method: 'post',
    data: params
  })
}

export function downloadFile(fileName) {
  return transfer({
    url: '/dl?file=' + fileName,
    method: 'get',
    data: {}
  })
}