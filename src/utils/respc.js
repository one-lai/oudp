const respCode = {
  USER_NOT_LOGGED_IN: { code: 20001, msg: '用户未登录' },
  USER_HAS_EXISTED: { code: 20005, msg: '用户已存在' }
}

export default respCode
