import Vue from 'vue'
import Vuex from 'vuex'
import app from './modules/app'
import tagsView from './modules/tagsView'
import user from './modules/user'
import zoneCache from './modules/zoneCache'
import getters from './getters'
import month from './modules/month'
import updata from './modules/update'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    app,
    tagsView,
    user,
    month,
    zoneCache,
    updata
  },
  getters
})

export default store