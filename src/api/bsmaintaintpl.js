import request from '@/utils/request'
import transfer from '@/utils/transfer'

export function getList(params) {
  return request({
    url: '/api/bsmaintaintpl/bsMaintainTpl/list',
    method: 'post',
    data: params,
    headers: { 'Content-Type': 'application/json' }
  })
}

export function saveList(params) {
  console.log(params)
  return request({
    url: '/api/bsmaintaintpl/bsMaintainTpl/save',
    method: 'post',
    data: params,
    headers: { 'Content-Type': 'application/json' }
  })
}

export function deleteItem(params) {
  return request({
    url: '/api/bsmaintaintpl/bsMaintainTpl/delete',
    method: 'post',
    data: params,
    headers: { 'Content-Type': 'application/json' }
  })
}

export function deleteItems(params) {
  console.log(params)
  return request({
    url: '/api/bsmaintaintpl/bsMaintainTpl/deleteAll',
    method: 'post',
    data: params,
    headers: { 'Content-Type': 'application/json' }
  })
}

export function exportData(params) {
  return transfer({
    method: 'post',
    url: '/api/bsmaintaintpl/bsMaintainTpl/export',
    data: params
  })
}

export function getTemplate() {
  return transfer({
    method: 'post',
    url: '/api/bsmaintaintpl/bsMaintainTpl/import/template',
    data: ''
  })
}

export function sift(params) {
  return request({
    url: '/api/bsmaintaintpl/bsMaintainTpl/sift',
    method: 'post',
    data: params
  })
}
