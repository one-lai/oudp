import request from '@/utils/request'

export function login() {
  return request({
    url: '/relogin',
    method: 'post',
    data: {}
  })
}

// 获取登录地址
export function getStub() {
  return request({
    url: '/stub',
    method: 'post',
    data: {}
  })
}

export function getInfo(token) {
  return request({
    url: '/user/info',
    method: 'post',
    params: {
      token
    }
  })
}

export function logout() {
  return request({
    url: '/logout',
    method: 'post'
  })
}

// 获取登录用户信息
export function logon() {
  return request({
    url: '/user/logon',
    method: 'post',
    data: {}
  })
}