import request from '@/utils/request'
import transfer from '@/utils/transfer'

export function getList(params) {
  return request({
    url: '/api/bsgdtemplate/bsGdTemplate/list',
    method: 'post',
    data: params,
    headers: { 'Content-Type': 'application/json' }
  })
}

export function getTemplate(params) {
  return request({
    url: '/api/bsgdtemplate/bsGdTemplate/getTemplate',
    method: 'post',
    data: params,
    headers: { 'Content-Type': 'application/json' }
  })
}

export function saveTemplate(params) {
  return request({
    url: '/api/bsgdtemplate/bsGdTemplate/saveTemplate',
    method: 'post',
    data: params,
    headers: { 'Content-Type': 'application/json' }
  })
}

export function getFields(params) {
  return request({
    url: '/api/bsgdtemplate/bsGdTemplate/getFields',
    method: 'post',
    data: params,
    headers: { 'Content-Type': 'application/json' }
  })
}

export function saveDetail(params) {
  return request({
    url: '/api/bsgdtemplate/bsGdTemplate/saveDetail',
    method: 'post',
    data: params,
    headers: { 'Content-Type': 'application/json' }
  })
}

export function saveList(params) {
  return request({
    url: '/api/bsgrade/bsGrade/save',
    method: 'post',
    data: params,
    headers: { 'Content-Type': 'application/json' }
  })
}

export function deleteItem(params) {
  return request({
    url: '/api/bsgrade/bsGrade/delete',
    method: 'post',
    data: params,
    headers: { 'Content-Type': 'application/json' }
  })
}

export function seeDetail(params) {
  console.log(params)
  return request({
    url: '/api/bsgrade/bsGrade/detail',
    method: 'post',
    data: params,
    headers: { 'Content-Type': 'application/json' }
  })
}

