import request from '@/utils/request'

export function addUser(params) {
  console.log(params)
  return request({
    url: '/api/bsuser/bsUser/addUser',
    method: 'post',
    data: params,
    headers: { 'Content-Type': 'application/json' }
  })
}
