import {
  getZone
} from '@/api/tool'
import {
  getKey
} from '@/utils/translate'
const zoneCache = {
  state: {
    zone: []
  },

  mutations: {
    SET_ZONE: (state, zone) => {
      let hasKey = state.zone.length > 0 && state.zone.some(v => {
        return v.key == zone.key
      })
      if (!hasKey) {
        state.zone.push(zone)
      }
    }
  },

  actions: {
    getKey(keyObj) {
      if (keyObj) {
        return (
          "__key" +
          "_p_" +
          keyObj.province.id +
          "_c_" +
          keyObj.city.id +
          "_r_" +
          keyObj.region.id
        );
      } else {
        return "__key";
      }
    },
    getZoneCache({
      commit,
      state
    }, key) {
      return new Promise((resolve, reject) => {
        getZone(key).then(response => {
          const data = response.result
          if (data) {
            commit('SET_ZONE', {
              key: getKey(key),
              value: data
            })
          } else {}
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    }
  }
}

export default zoneCache