// 数据字典，value=>label
export function translate(val, maps) {
  if (typeof (maps) !== 'object') {
    return val
  }
  var map = maps.filter(v => {
    if (v.value === val) {
      return v
    }
  })
  if (map && map.length > 0) {
    return map[0].label
  } else {
    return val
  }
}

// 根据父子字段获取字符
export function transParentChild(parentVal, childVal, maps) {
  if (typeof (maps) !== 'object') {
    return parentVal
  }
  // console.log('parentVal==>', parentVal, childVal)
  var parentList = maps.filter(v => {
    if (v.value === parentVal) {
      return v
    }
  })
  // console.log('parentList==>', JSON.stringify(parentList))
  if (parentList[0] === undefined) {
    return parentVal
  }

  var childList = parentList[0].childen.filter(v => {
    if (v.value === childVal) {
      // console.log('v--->', cLabel, JSON.stringify(v.childen))
      return v
    }
  })

  // console.log('childList==>', JSON.stringify(childList))
  if (childList && childList.length > 0) {
    // return parentList[0].label + '_' + childList[0].label
    return childList[0].label
  } else {
    return ''
  }
}

export function extract(fileList) {
  var uploadedFile = []
  if (fileList) {
    fileList.forEach(v => {
      try {
        if (v.response) {
          uploadedFile.push({
            name: v.name,
            url: '/fms/download?s=' + v.response.data[0].share
          })
        } else {
          uploadedFile.push({
            name: v.name,
            url: v.url
          })
        }
      } catch (e) {
        console.log(e)
      }
    })
  }
  return JSON.stringify(uploadedFile)
}

export function extractFile(file) {
  var uploadedFile = {}
  try {
    if (file.response) {
      uploadedFile = {
        name: file.name,
        url: '/fms/download?s=' + file.response.data[0].share
      }
    } else {
      uploadedFile = {
        name: file.name,
        url: file.url
      }
    }
  } catch (e) {
    console.log(e)
  }
  return JSON.stringify(uploadedFile)
}

// 数据字典
export function unified(dict) {
  if (Array.isArray(dict)) {
    return dict.map(v => {
      return {
        value: v.dictKey,
        label: v.dictValue
      }
    })
  } else {
    return []
  }
}

export function getKey(keyObj) {
  if (keyObj) {
    return (
      "__key" +
      "_p_" +
      keyObj.province.id +
      "_c_" +
      keyObj.city.id +
      "_r_" +
      keyObj.region.id
    );
  } else {
    return "__key";
  }
}