import request from '@/utils/request'

export function getIndexData() {
  return request({
    url: '/api/workbench/workBench/indexData',
    method: 'post',
    data: '',
    headers: { 'Content-Type': 'application/json' }
  })
}

/**
 *param的参数 organizationType=1维保，organizationType=2使用单位
 */
export function getOrganizationData(param) {
  return request({
    url: '/api/workbench/workBench/organizationData',
    method: 'post',
    data: param,
    headers: { 'Content-Type': 'application/json' }
  })
}

export function getElevatorData() {
  return request({
    url: '/api/workbench/workBench/bselevatorData',
    method: 'post',
    data: '',
    headers: { 'Content-Type': 'application/json' }
  })
}

export function getUserPlaceData() {
  return request({
    url: '/api/workbench/workBench/bsUserPlaceData',
    method: 'post',
    data: '',
    headers: { 'Content-Type': 'application/json' }
  })
}

export function getOrganizationDataDaily(param) {
  return request({
    url: '/api/workbench/workBench/organizationDataDaily',
    method: 'post',
    data: param,
    headers: { 'Content-Type': 'application/json' }
  })
}

export function getElevatorDataDaily(param) {
  return request({
    url: '/api/workbench/workBench/bselevatorDataDaily',
    method: 'post',
    data: param,
    headers: { 'Content-Type': 'application/json' }
  })
}

export function getUserPlaceDataDaily(param) {
  return request({
    url: '/api/workbench/workBench/bsUserPlaceDataDaily',
    method: 'post',
    data: param,
    headers: { 'Content-Type': 'application/json' }
  })
}
