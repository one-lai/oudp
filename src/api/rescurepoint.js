import request from '@/utils/request'
import transfer from '@/utils/transfer'

export function getList(params) {
  return request({
    url: '/api/bsrescurepoint/bsRescurePoint/list',
    method: 'post',
    data: params,
    headers: {'Content-Type': 'application/json'}
  })
}

export function saveList(params) {
  console.log(params)
  return request({
    url: '/api/bsrescurepoint/bsRescurePoint/save',
    method: 'post',
    data: params,
    headers: {'Content-Type': 'application/json'}
  })
}


export function seeDetail(params) {
  console.log(params)
  return request({
    url: '/api/bsrescurepoint/bsRescurePoint/detail',
    method: 'post',
    data: params,
    headers: { 'Content-Type': 'application/json' }
  })
}

export function deleteItem(params) {
  return request({
    url: '/api/bsrescurepoint/bsRescurePoint/delete',
    method: 'post',
    data: params,
    headers: {'Content-Type': 'application/json'}
  })
}

export function deleteItems(params) {
  console.log(params)
  return request({
    url: '/api/bsrescurepoint/bsRescurePoint/deleteAll',
    method: 'post',
    data: params,
    headers: {'Content-Type': 'application/json'}
  })
}

export function exportData(params) {
  return transfer({
    method: 'post',
    url: '/api/bsrescurepoint/bsRescurePoint/export',
    data: params
  })
}

export function exportDataExactly(params) {
  return transfer({
    method: 'post',
    url: '/api/bsrescurepoint/bsRescurePoint/exportExactly',
    data: params
  })
}

export function getTemplate() {
  return transfer({
    method: 'post',
    url: '/api/bsrescurepoint/bsRescurePoint/import/template',
    data: ''
  })
}

export function getTemplateTechnician() {
  return transfer({
    method: 'post',
    url: '/api/bsrescurepoint/bsRescurePoint/import/templateTechnician',
    data: ''
  })
}

export function importExcel(params) {
  return request({
    url: '/api/bsrescurepoint/bsRescurePoint/import',
    method: 'post',
    data: params,
    headers: { 'Content-Type': 'multipart/form-data' }
  })
}

export function checkContactNum(params) {
  return request({
    url: '/api/bsrescurepoint/bsRescurePoint/checkContactNum',
    method: 'post',
    data: params,
    headers: { 'Content-Type': 'application/json' }
  })
}

export function sift(params) {
  return request({
    url: '/api/bsrescurepoint/bsRescurePoint/sift',
    method: 'post',
    data: params
  })
}

