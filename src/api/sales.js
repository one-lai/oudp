import request from '@/utils/request'
import transfer from '@/utils/transfer'

export function getList(params) {
  return request({
    url: '/api/sales/bsElevatorSales/list',
    method: 'post',
    data: params,
    headers: { 'Content-Type': 'application/json' }
  })
}

export function saveList(params) {
  return request({
    url: '/api/sales/bsElevatorSales/save',
    method: 'post',
    data: params,
    headers: { 'Content-Type': 'application/json' }
  })
}

export function deleteItem(params) {
  return request({
    url: '/api/sales/bsElevatorSales/delete',
    method: 'post',
    data: params,
    headers: { 'Content-Type': 'application/json' }
  })
}

export function deleteItems(params) {
  console.log(params)
  return request({
    url: '/api/sales/bsElevatorSales/deleteAll',
    method: 'post',
    data: params,
    headers: { 'Content-Type': 'application/json' }
  })
}

export function exportData(params) {
  return transfer({
    method: 'post',
    url: '/api/sales/bsElevatorSales/export',
    data: params
  })
}

export function exportDataExactly(params) {
  return transfer({
    method: 'post',
    url: '/api/sales/bsElevatorSales/exportExactly',
    data: params
  })
}

export function getTemplate() {
  return transfer({
    method: 'post',
    url: '/api/sales/bsElevatorSales/import/template',
    data: ''
  })
}

export function sift(params) {
  return request({
    url: '/api/sales/bsElevatorSales/sift',
    method: 'post',
    data: params
  })
}
