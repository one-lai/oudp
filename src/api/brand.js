import request from '@/utils/request'
import transfer from '@/utils/transfer'

export function getDataList(params) {
  return request({
    url: '/api/bsbrand/bsBrand/list',
    method: 'post',
    data: params
  })
}

export function deleteData(params) {
  return request({
    url: '/api/bsbrand/bsBrand/delete',
    method: 'post',
    data: params
  })
}

export function deleteItems(params) {
  return request({
    url: '/api/bsbrand/bsBrand/deleteAll',
    method: 'post',
    data: params,
    headers: { 'Content-Type': 'application/json' }
  })
}

export function saveData(params) {
  return request({
    url: '/api/bsbrand/bsBrand/save',
    method: 'post',
    data: params
  })
}

export function exportData(params) {
  return transfer({
    method: 'post',
    url: '/api/bsbrand/bsBrand/export',
    data: params
  })
}

/** 导出选中 */
export function exportDataExactly(params) {
  return transfer({
    method: 'post',
    url: '/api/bsbrand/bsBrand/exportExactly',
    data: params
  })
}

export function getTemplate() {
  return transfer({
    method: 'post',
    url: '/api/bsbrand/bsBrand/import/template',
    data: ''
  })
}

export function sift(params) {
  return request({
    url: '/api/bsbrand/bsBrand/sift',
    method: 'post',
    data: params
  })
}
