import request from '@/utils/request'
import transfer from '@/utils/transfer'

export function getManufacturerAll() {
  return request({
    url: '/api/bsorganization/bsOrganization/manufacturer',
    method: 'post',
    headers: { 'Content-Type': 'application/json' }
  })
}

export function getList(params) {
  return request({
    url: '/api/product/bsProduct/list',
    method: 'post',
    data: params,
    headers: { 'Content-Type': 'application/json' }
  })
}

export function saveList(params) {
  return request({
    url: '/api/product/bsProduct/save',
    method: 'post',
    data: params,
    headers: { 'Content-Type': 'application/json' }
  })
}

export function deleteItem(params) {
  return request({
    url: '/api/product/bsProduct/delete',
    method: 'post',
    data: params,
    headers: { 'Content-Type': 'application/json' }
  })
}

export function deleteItems(params) {
  return request({
    url: '/api/product/bsProduct/deleteAll',
    method: 'post',
    data: params,
    headers: { 'Content-Type': 'application/json' }
  })
}

export function exportData(params) {
  return transfer({
    method: 'post',
    url: '/api/product/bsProduct/export',
    data: params
  })
}

/** 导出选中 */
export function exportDataExactly(params) {
  return transfer({
    method: 'post',
    url: '/api/product/bsProduct/exportExactly',
    data: params
  })
}

export function getTemplate() {
  return transfer({
    method: 'post',
    url: '/api/product/bsProduct/import/template',
    data: ''
  })
}

export function sift(params) {
  return request({
    url: '/api/product/bsProduct/sift',
    method: 'post',
    data: params
  })
}

export function getManufacturer(params) {
  return request({
    url: '/api/product/bsProduct/manufacturer',
    method: 'post',
    data: params,
    headers: {
      'Content-Type': 'application/json'
    }
  })
}