/**
 * Created by jiachenpan on 16/11/18.
 */

export function parseTime(time, cFormat) {
  if (arguments.length === 0) {
    return null
  }
  const format = cFormat || '{y}-{m}-{d} {h}:{i}:{s}'
  let date
  if (typeof time === 'object') {
    date = time
  } else {
    if (('' + time).length === 10) time = parseInt(time) * 1000
    date = new Date(time)
  }
  const formatObj = {
    y: date.getFullYear(),
    m: date.getMonth() + 1,
    d: date.getDate(),
    h: date.getHours(),
    i: date.getMinutes(),
    s: date.getSeconds(),
    a: date.getDay()
  }
  const time_str = format.replace(/{(y|m|d|h|i|s|a)+}/g, (result, key) => {
    let value = formatObj[key]
    if (key === 'a') return ['一', '二', '三', '四', '五', '六', '日'][value - 1]
    if (result.length > 0 && value < 10) {
      value = '0' + value
    }
    return value || 0
  })
  return time_str
}

export function formatTime(time, option) {
  time = +time * 1000
  const d = new Date(time)
  const now = Date.now()

  const diff = (now - d) / 1000

  if (diff < 30) {
    return '刚刚'
  } else if (diff < 3600) { // less 1 hour
    return Math.ceil(diff / 60) + '分钟前'
  } else if (diff < 3600 * 24) {
    return Math.ceil(diff / 3600) + '小时前'
  } else if (diff < 3600 * 24 * 2) {
    return '1天前'
  }
  if (option) {
    return parseTime(time, option)
  } else {
    return d.getMonth() + 1 + '月' + d.getDate() + '日' + d.getHours() + '时' + d.getMinutes() + '分'
  }
}
export function debounce(func, wait, immediate) {
  let timeout, args, context, timestamp, result

  const later = function () {
    // 据上一次触发时间间隔
    const last = +new Date() - timestamp

    // 上次被包装函数被调用时间间隔last小于设定时间间隔wait
    if (last < wait && last > 0) {
      timeout = setTimeout(later, wait - last)
    } else {
      timeout = null
      // 如果设定为immediate===true，因为开始边界已经调用过了此处无需调用
      if (!immediate) {
        result = func.apply(context, args)
        if (!timeout) context = args = null
      }
    }
  }

  return function (...args) {
    context = this
    timestamp = +new Date()
    const callNow = immediate && !timeout
    // 如果延时不存在，重新设定延时
    if (!timeout) timeout = setTimeout(later, wait)
    if (callNow) {
      result = func.apply(context, args)
      context = args = null
    }

    return result
  }
}
export const objEqual = (obj1, obj2) => {
  const keysArr1 = Object.keys(obj1)
  const keysArr2 = Object.keys(obj2)
  if (keysArr1.length !== keysArr2.length) return false
  else if (keysArr1.length === 0 && keysArr2.length === 0) return true
  /* eslint-disable-next-line */
  else return !keysArr1.some(key => obj1[key] != obj2[key])
}

export const routeEqual = (route1, route2) => {
  const params1 = route1.params || {}
  const params2 = route2.params || {}
  const query1 = route1.query || {}
  const query2 = route2.query || {}
  return (route1.name === route2.name) && objEqual(params1, params2) && objEqual(query1, query2)
}

/**
 * set or clear keepAlive
 * @param {*} routes 
 * @param {*} id 
 * @param {*} state 
 */
export function toggleRouteAlive(routes, id, state) {
  var _route = null;
  if (!Array.isArray(routes)) {
    return _route;
  }
  let i;
  for (i = 0; i < routes.length; i++) {
    let route = routes[i];
    if (_route != null) {
      break;
    }
    try {
      if (route.meta && route.name == id) {
        _route = route;
        break;
      } else {
        if (typeof (state) != 'undefined') {
          try {
            route.meta.keepAlive = !state;
          } catch (e) {}
        }
      }
      if (route.children && route.children.length > 0) {
        _route = toggleRouteAlive(route.children, id, state);
      }
    } catch (e) {
      console.log(e);
    }
  }
  if (typeof (state) != 'undefined' && _route) {
    _route.meta.keepAlive = state;
  }
  return _route;
}

/**
 * set or clear keepAlive of all
 * @param {*} routes 
 * @param {*} state 
 */
export function toggleRouteAliveAll(routes, state) {
  var _route = null;
  if (!Array.isArray(routes)) {
    return _route;
  }
  let i;
  for (i = 0; i < routes.length; i++) {
    let route = routes[i];
    if (_route != null) {
      break;
    }
    try {
      if (typeof (state) != 'undefined') {
        try {
          route.meta.keepAlive = !state;
        } catch (e) {}
      }
      if (route.children && route.children.length > 0) {
        toggleRouteAliveAll(route.children, state);
      }
    } catch (e) {
      console.log(e);
    }
  }
}