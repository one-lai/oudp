import request from '@/utils/request'
import transfer from '@/utils/transfer'

export function getList(params) {
  return request({
    url: '/api/bsusepace/bsUsePlace/list',
    method: 'post',
    data: params,
    headers: {'Content-Type': 'application/json'}
  })
}

export function saveList(params) {
  return request({
    url: '/api/bsusepace/bsUsePlace/save',
    method: 'post',
    data: params,
    headers: {'Content-Type': 'application/json'}
  })
}

export function deleteItem(params) {
  return request({
    url: '/api/bsusepace/bsUsePlace/delete',
    method: 'post',
    data: params,
    headers: {'Content-Type': 'application/json'}
  })
}

export function deleteItems(params) {
  console.log(params)
  return request({
    url: '/api/bsusepace/bsUsePlace/deleteAll',
    method: 'post',
    data: params,
    headers: {'Content-Type': 'application/json'}
  })
}

export function exportData(params) {
  return transfer({
    method: 'post',
    url: '/api/bsusepace/bsUsePlace/export',
    data: params
  })
}

export function getTemplate() {
  return transfer({
    method: 'post',
    url: '/api/bsusepace/bsUsePlace/import/template',
    data: ''
  })
}

/** 导出选中 */
export function exportDataExactly(params) {
  return transfer({
    method: 'post',
    url: '/api/bsusepace/bsUsePlace/exportExactly',
    data: params
  })
}

export function sift(params) {
  return request({
    url: '/api/bsusepace/bsUsePlace/sift',
    method: 'post',
    data: params
  })
}
