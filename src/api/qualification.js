import request from '@/utils/request'
import transfer from '@/utils/transfer'

export function getQualificationList(params) {
  return request({
    url: '/api/qualification/list',
    method: 'post',
    data: params
  })
}

export function deleteData(params) {
  return request({
    url: '/api/qualification/delete',
    method: 'post',
    data: params
  })
}

export function deleteItems(params) {
  return request({
    url: '/api/qualification/deleteAll',
    method: 'post',
    data: params,
    headers: { 'Content-Type': 'application/json' }
  })
}

export function saveData(params) {
  return request({
    url: '/api/qualification/save',
    method: 'post',
    data: params
  })
}

export function exportData(params) {
  return transfer({
    method: 'post',
    url: '/api/qualification/export',
    data: params
  })
}

/** 导出选中 */
export function exportDataExactly(params) {
  return transfer({
    method: 'post',
    url: '/api/qualification/exportExactly',
    data: params
  })
}

export function getTemplate() {
  return transfer({
    method: 'post',
    url: '/api/qualification/import/template',
    data: ''
  })
}
