import Vue from 'vue'

window.qs = require('qs')
window.axios = require('axios')
window.axios.defaults.headers.post['Content-Type'] = 'application/json'

import 'normalize.css/normalize.css' // A modern alternative to CSS resets

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
// import locale from 'element-ui/lib/locale/lang/en' // lang i18n

import ElementExt from './components/element-ext'

import '@/styles/index.scss' // global css

import App from './App'
import router from './router'
import store from './store'

import '@/icons' // icon
import '@/permission' // permission control

import {
  calculateHeight
} from '@/utils/css'


// import TopBtn from '@/components/Ogrid/TopBtn/index.vue'

//import OriocComponents from 'orioc-components'



// Vue.component('topBtn', TopBtn)


// Vue.use(ElementUI, { locale })
Vue.use(ElementUI)

Vue.use(ElementExt)
//Vue.use(OriocComponents);

Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App),
  data: {
    eventHub: new Vue()
  }
})