import request from '@/utils/request'
import transfer from '@/utils/transfer'

export function getList(params) {
  return request({
    url: '/api/bselevator/bsElevator/list',
    method: 'post',
    data: params,
    headers: { 'Content-Type': 'application/json' }
  })
}

export function saveList(params) {
  console.log(params)
  return request({
    url: '/api/bselevator/bsElevator/save',
    method: 'post',
    data: params,
    headers: { 'Content-Type': 'application/json' }
  })
}

export function deleteItem(params) {
  return request({
    url: '/api/bselevator/bsElevator/delete',
    method: 'post',
    data: params,
    headers: { 'Content-Type': 'application/json' }
  })
}

export function deleteItems(params) {
  console.log(params)
  return request({
    url: '/api/bselevator/bsElevator/deleteAll',
    method: 'post',
    data: params,
    headers: { 'Content-Type': 'application/json' }
  })
}

export function seeDetail(params) {
  return request({
    url: '/api/bselevator/bsElevator/detail',
    method: 'post',
    data: params,
    headers: { 'Content-Type': 'application/json' }
  })
}

export function exportData(params) {
  return transfer({
    method: 'post',
    url: '/api/bselevator/bsElevator/export',
    data: params
  })
}

export function exportDataExactly(params) {
  return transfer({
    method: 'post',
    url: '/api/bselevator/bsElevator/exportExactly',
    data: params
  })
}

export function getTemplate() {
  return transfer({
    method: 'post',
    url: '/api/bselevator/bsElevator/import/template',
    data: ''
  })
}

export function getUseUnitMaintenanceList() {
  return request({
    url: '/api/bsorganization/bsOrganization/getUseUnitMaintenanceList',
    method: 'post',
    headers: { 'Content-Type': 'application/json' }
  })
}

export function getBsOrganizationList(params) {
  return request({
    url: '/api/bsorganization/bsOrganization/list',
    method: 'post',
    data: params,
    headers: { 'Content-Type': 'application/json' }
  })
}

export function getprjList(params) {
  return request({
    url: '/api/bsusepace/bsUsePlace/list',
    method: 'post',
    data: params,
    headers: { 'Content-Type': 'application/json' }
  })
}

export function getPlaceChildenByType(params) {
  return request({
    url: '/api/bsusepace/bsUsePlace/getPlaceChildenByType',
    method: 'post',
    data: params,
    headers: { 'Content-Type': 'application/json' }
  })
}

export function placeDetail(params) {
  return request({
    url: '/api/bsusepace/bsUsePlace/detail',
    method: 'post',
    data: params,
    headers: { 'Content-Type': 'application/json' }
  })
}

export function qrCode(params) {
  return request({
    url: '/api/bsusepace/bsUsePlace/createTwoDimensionCode',
    method: 'post',
    data: params,
    headers: { 'Content-Type': 'application/json' }
  })
}

export function sift(params) {
  return request({
    url: '/api/bselevator/bsElevator/sift',
    method: 'post',
    data: params
  })
}


export function getOrganizationById(params) {
  return request({
    url: '/api/bsorganization/bsOrganization/detail',
    method: 'post',
    data: params,
    headers: {'Content-Type': 'application/json'}
  })
}

export function getObjByRegistrationCode(params) {
  return request({
    url: '/api/bselevator/bsElevator/getObjByRegistrationCode',
    method: 'post',
    data: params,
    headers: {'Content-Type': 'application/json'}
  })
}

export function getObjByPositionCode(params) {
  return request({
    url: '/api/bselevator/bsElevator/getObjByPositionCode',
    method: 'post',
    data: params,
    headers: {'Content-Type': 'application/json'}
  })
}

