
export default {
  functional: true,
  props: ['row', 'col', 'column'],
  render (h, { props: { row, col }, _v: text }) {
    const { formatter } = col
    col.property = col.prop
    const v = formatter && formatter(row, col, row[col.prop]) || row[col.prop]
    // 解决 umd 打包 text 渲染不出来的问题，需要转成 Vnode
    return text && text(v) || v
  }
}
